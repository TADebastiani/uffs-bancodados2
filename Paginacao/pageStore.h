#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// estrutura de cabeçalho
typedef struct Header {
	int memFree; // limpar memoria da pagina
	int next; // indica onde sera inserido o proximo elemento na página
	int qtdItems; // quantidade de itens
} header;

typedef struct Item {
	int offset;
	int totalLen; // tamanho total do elemento 
	int writed; 
} item;

typedef struct Attribute { // atributo da tabela sql, coluna da tabela sql
	char name[15]; // nome da coluna da tabela sql
	int size; // tamanho da coluna da tabela sql
	char type; //tipo da coluna da tabela sql = int, char e varchar
	int primary; // se campo é Primar Key (1 - true, 0 - false)
} attribute;

char *getTableName(char *sql, int *error);

void buildHeader(char *sql, char *tableName, int qtdPages);

void getAllAtributes(char *sql, char *attributes);

void leArquivo(char *tableName);

char *getTableFields(char *sql);

int indexOf(char *string, char find);

int lastIndexOf(char *string, char find);

int startsWith(const char *str, const char *pre);